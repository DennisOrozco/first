/**
 * Created by Jonathan on 2017-09-25.
 */

public class Alignement {

    private int score;
    private int chevauchement;
    private String seq1;
    private String seq2;
    private String alignedSeq1;
    private String alignedSeq2;

    //Getters
    public int getScore(){return this.score;}

    public Alignement (String seq1, String seq2, int matchValue, int mismatchValue, int indelValue){

        this.seq1 = seq1;
        this.seq2 = seq2;
        this.alignedSeq1 = "";
        this.alignedSeq2 = "";
        this.chevauchement = 0;

        aligneSequences(matchValue, mismatchValue, indelValue);
    }

    //Fonction qui aligne 2 séquences et retourne le score
    public int aligneSequences(int matchValue, int mismatchValue, int indelValue){

        int lr = seq1.length()+1;
        int lc = seq2.length()+1;

        int[][] matrixScore = new int[lr][lc];
        String[][] matrixPred = new String[lr][lc];

        int posi = 0;
        int posj = 0;
        int score = 0;
        int scoreTmp;

        for(int i=0; i<lr; i++){
            for(int j=0; j<lc; j++){

                matrixScore[i][j] = 0;
                matrixPred[i][j] = "";

                //Si on est dans la première rangé ou colonne, on ne fait qu'initialiser à 0 et continuer
                if(j==0 || i==0)
                    continue;

                //indel seq1
                scoreTmp = matrixScore[i-1][j] + indelValue;

                if(scoreTmp>0){
                    matrixScore[i][j] = scoreTmp;
                    matrixPred[i][j] = "u";
                }

                //indel seq2
                scoreTmp = matrixScore[i][j-1] + indelValue;

                if(scoreTmp == matrixScore[i][j])
                    matrixPred[i][j] += "l";

                else if(scoreTmp > matrixScore[i][j]){
                    matrixScore[i][j] = scoreTmp;
                    matrixPred[i][j] = "l";
                }

                //Diagonal : match ou mismatch
                int p = (seq1.charAt(i-1) == seq2.charAt(j-1))? matchValue : mismatchValue;
                scoreTmp =  matrixScore[i-1][j-1] + p;

                if(scoreTmp == matrixScore[i][j])
                    matrixPred[i][j] += "d";

                else if(scoreTmp > matrixScore[i][j]) {
                    matrixScore[i][j] = scoreTmp;
                    matrixPred[i][j] = "d";
                }

                if(j == lc-1 || i == lr-1){
                    if(score < matrixScore[i][j]){
                        score = matrixScore[i][j];
                        posi = i;
                        posj = j;
                    }
                }
            }
        }

        this.score = score;

        retrouverAlignement(matrixPred, posi, posj);

        return score;
    }

    //Fonction qui retrouve l'alignement des séquence et le chauvechement
    private void retrouverAlignement(String[][] matricePred, int posi, int posj){

        int i = posi;
        int j = posj;

        String pred = matricePred[i][j];
        char direction;
        int chevauchement = 0;

        while(pred != ""){

            direction = pred.charAt((int) Math.floor(pred.length()*Math.random()));

            chevauchement++;

            switch(direction){
                case 'u':
                    this.alignedSeq1 = "-" + this.alignedSeq1;
                    this.alignedSeq2 = seq2.charAt(j-1) + this.alignedSeq2;
                    i--;
                    break;
                case 'l':
                    this.alignedSeq1 = seq1.charAt(i-1) + this.alignedSeq1;
                    this.alignedSeq2 = "-" + this.alignedSeq2;
                    j--;
                    break;
                case 'd':
                    this.alignedSeq1 = seq1.charAt(i-1) + this.alignedSeq1;
                    this.alignedSeq2 = seq2.charAt(j-1) + this.alignedSeq2;
                    i--;
                    j--;
                    break;
            }

            pred = matricePred[i][j];
        }

        this.chevauchement = chevauchement;

        if(posi == this.seq1.length()-1) {

            int lp = this.seq1.length()-chevauchement;
            String prefixe1 = this.seq1.substring(0, lp);
            String prefixe2 = "_";

            while(prefixe2.length()<lp)
                prefixe2 += "-";

            this.alignedSeq1 = prefixe1 + this.alignedSeq1;
            this.alignedSeq2 = prefixe2 + this.alignedSeq2;

        }
        else{

            int lp = this.seq2.length()-chevauchement;
            String prefixe2 = this.seq2.substring(0, lp);
            String prefixe1 = "-";

            while(prefixe1.length()<lp)
                prefixe1 += "-";

            this.alignedSeq1 = prefixe1 + this.alignedSeq1;
            this.alignedSeq2 = prefixe2 + this.alignedSeq2;
        }

        return;
    }

    //
    public String toString(){

        String alignement = "sequence1 : " + this.alignedSeq1 + "\n";
        alignement += "sequence2 : " + this.alignedSeq2 + "\n";
        alignement += "score : " + this.score + "\n";
        alignement += "chevauchement : " + this.chevauchement;

        return alignement;
    }
}