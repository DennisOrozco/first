/**
 * Created by dennisorozco on 17-09-24.
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    public static void main (String [] args){
        /*try {
            BufferedReader br = new BufferedReader(new FileReader("/Users/dennisorozco/TP-1-BioInfo/src/sequences"));
            String line1;
            String line2;
            try {
                line1 = br.readLine();
                System.out.println(line1);
                System.out.println("ligne 1 de longueur: "+line1.length());
                line2 = br.readLine();
                System.out.println(line2);
                System.out.println("ligne 2 de longueur: "+line2.length());
                br.close();
                Sequences alignement = new Sequences(line1,line2);
                alignement.trie();
            } catch (IOException e) {
                System.out.println("le fichier ne contient pas de sequences");
            }
        } catch (FileNotFoundException ex) {
            System.out.println("le fichier n'existe pas");
        }*/

        String test1 = "ACCACAGTAGATAGATTGAGGA";
        String test2 = "TAGATGAGGACATGAGGTAGAT";

        Alignement aligneTest = new Alignement(test1, test2, 4,-4,-8);

        System.out.println("Score : " + aligneTest.getScore());
        System.out.println(aligneTest.toString());
    }
}
